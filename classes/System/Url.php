<?php

namespace Knowledge\System;

class Url
{
    private static $instance;

    public $method = '';
    public $token;
    public $refresh_token;

    public $requestUri = [];
    public $requestParams = [];

    public static function getInstance()
    {
        if(self::$instance === null) {
            self::$instance = new Url();
        }
        return self::$instance;
    }

    private function __construct()
    {
        $uri = trim($_SERVER['REQUEST_URI'],'/');
        if (strpos($uri, '?')) {
            $uri = substr($uri, 0, strpos($uri, '?'));
        }
        $this->requestUri = explode('/', $uri);

        $this->requestParams = $_REQUEST;
        $this->method = $_SERVER['REQUEST_METHOD'];
        if ($this->method == 'POST' && array_key_exists('HTTP_X_HTTP_METHOD', $_SERVER)) {
            if ($_SERVER['HTTP_X_HTTP_METHOD'] == 'DELETE') {
                $this->method = 'DELETE';
            } else if ($_SERVER['HTTP_X_HTTP_METHOD'] == 'PUT') {
                $this->method = 'PUT';
            } else {
                throw new Exception("Unexpected Header");
            }
        }
        if (isset($_SERVER['HTTP_TOKEN'])) {
            $this->token = $_SERVER['HTTP_TOKEN'];
        }
        if (isset($_SERVER['HTTP_REFRESHTOKEN'])) {
            $this->refresh_token = $_SERVER['HTTP_REFRESHTOKEN'];
        }


    }

}