<?php

namespace Knowledge\Controllers;

use Knowledge\System\Url;

class Api
{
    protected $oUrl;
    protected $action;

    function __construct(Url $oUrl)
    {
        $this->oUrl = $oUrl;
    }

    function run()
    {
        $this->action = $this->getAction($this->oUrl);
        if (method_exists($this, $this->action)) {
            return $this->{$this->action}();
        } else {
            throw new RuntimeException('Invalid Method', 405);
        }
    }
    protected function response($data, $status = 500) {
        header("HTTP/1.1 " . $status . " " . $this->requestStatus($status));
        return json_encode($data);
    }

    private function requestStatus($code) {
        $status = array(
            200 => 'OK',
            404 => 'Not Found',
            405 => 'Method Not Allowed',
            500 => 'Internal Server Error',
        );
        return ($status[$code])?$status[$code]:$status[500];
    }


    public function getOUrl()
    {
        return $this->oUrl;
    }
}