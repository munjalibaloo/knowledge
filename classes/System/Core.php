<?php

namespace Knowledge\System;

use Knowledge\System\Url;

class Core
{
    protected $routing;

    public function init()
    {
        $this->routing = self::getConfigRouting();
    }

    public function getRouteController() {
        $oUrl = Url::getInstance();

        if ($oUrl->requestUri) {
            foreach ($this->routing as $url => $apiName) {
                if ($oUrl->requestUri[0] == $url) {
                    $api = "Knowledge\Controllers\\".$apiName;
                    return new $api($oUrl);
                }
            }
        }
        return false;
    }
    static function getConfigRouting()
    {
        $path = str_replace('\\', '/', '__DIR__."../../config/')."routing.php";
        return require_once $path;
    }

}