<?php

namespace Knowledge\System;

use Knowledge\Models\User;

class Auth
{
    static public function getUserByUsernamePassword($username, $password_raw) {
        $mUser = new User();
        $oUser = $mUser->find(['username' => $username]);
        if ($oUser && password_verify($password_raw, $oUser['password'])) {
            return $oUser;
        } else {
            return false;
        }
    }


}