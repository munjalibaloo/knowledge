<?php
return [
    'host' => 'localhost',
    'port' => false,
    'user' => 'root',
    'password' => 'root',
    'database' => 'knowledge',
    'type' => 'mysqli',
    'charset' => 'utf8'
];
