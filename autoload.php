<?php
$namespaces = [
    "Knowledge\\" => "classes"
];

spl_autoload_register(function (string $class) use ($namespaces) {
    $prefix = strtok($class, '\\') . '\\';

    if (!array_key_exists($prefix, $namespaces)) {
        return;
    }
    $base_directory = $namespaces[$prefix];
    $class_rel = ltrim($class, $prefix);
    $path = str_replace('\\', '/', $class_rel) . '.php';
    require $base_directory . '/' . $path;
});