<?php

namespace Knowledge\Controllers;

use Knowledge\System\Token;
use Knowledge\Models\Student;

class UsersApi extends Api
{
    protected function getAction($oUrl)
    {
        $method = $oUrl->method;
        switch ($method) {
            case 'GET':
                return 'studentsAction';
                break;
            default:
                return null;
        }
    }

    public function studentsAction()
    {
        $oUrl = parent::getOUrl();
        if ($oUrl->token) {
            $oUser = Token::getUserByToken($oUrl->token);
            if ($oUser) {
                if (!isset($oUser['old'])) {

                    $aResponse = [];
                    $limit = 5;
                    $p = $oUrl->requestParams['page'] ?? 0;
                    if ($p == 0) {
                        $p = 1;
                    }
                    $mStudent = new Student();
                    $aStudents = $mStudent->getNextStudents($p, $limit);
                    if ($aStudents) {
                        $countAll = $mStudent->countAll();
                        $pages_count = ceil($countAll / $limit);
                        $aResponse['count_all'] = $countAll;
                        $aResponse['aStudents'] = $aStudents;
                        $aResponse['pages_count'] = $pages_count;
                    } else {
                        $aResponse['count_all'] = 0;
                        $aResponse['pages_count'] = 0;
                    }
                    $aResponse['limit'] = $limit;
                    $aResponse['page'] = $p;
                    return $this->response($aResponse, 200);
                } else {
                    return $this->response(["error" => "Invalid token (Old)"], 401);
                }
            } else {
                return $this->response(["error" => "Invalid token"], 401);
            }
        } else {
            return $this->response(["error" => "Token lost"], 401);
        }

    }
}