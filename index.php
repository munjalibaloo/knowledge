<?php
require_once "autoload.php";


$core = new Knowledge\System\Core();
$core->init();

$api = $core->getRouteController();
if ($api) {
    echo $api->run();
} else {
    echo json_encode([
        'error' => 404
    ]);
}

