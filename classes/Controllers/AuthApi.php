<?php

namespace Knowledge\Controllers;

use Knowledge\System\Auth;
use Knowledge\System\Token;

class AuthApi extends Api
{


    protected function getAction($oUrl)
    {
        $method = $oUrl->method;
        switch ($method) {
            case 'POST':
                return 'authAction';
                break;
            case 'GET':
                return 'checkAction';
                break;
            case 'DELETE':
                return 'logoutAction';
                break;
            default:
                return null;
        }
    }
    public function logoutAction()
    {
        $oUrl = parent::getOUrl();
        if ($oUrl->token) {
            Token::deleteUserTokenByToken($oUrl->token);
        }
        return $this->response(['ok' => 1], 200);
    }
    public function authAction()
    {
        $oUrl = parent::getOUrl();
        $username = $oUrl->requestParams['username'] ?? '';
        $password = $oUrl->requestParams['password'] ?? '';
        $remember = $oUrl->requestParams['remember'] ?? 0;
        if ($username && $password) {
            //Вынести в отдельный класс валидацию параметров
            $username = htmlspecialchars(strip_tags(trim($username)));
            $oUser = Auth::getUserByUsernamePassword($username, $password);
            if ($oUser) {
                $tokens = Token::genTokenForUser($oUser, $remember);
                return $this->response($tokens, 200);
            } else {
                return $this->response(["error" => "invalid username or password"], 401);
            }
        } else {
            return $this->response(["error" => "fill username and password"], 401);
        }

    }
    public function checkAction()
    {
        $oUrl = parent::getOUrl();
        if ($oUrl->token) {
            $oUser = Token::getUserByToken($oUrl->token);
            if ($oUser) {
                if (!isset($oUser['old'])) {
                    return $this->response(["id" => $oUser['id']], 200);
                } else {

                    $refresh_token = htmlspecialchars(strip_tags(trim($oUrl->refresh_token)));
                    $oUser = Token::getUserByRefreshToken($oUrl->token, $refresh_token);
                    if ($oUser) {
                        return $this->response(["id" => $oUser['id'], "token" => $oUser['tokens']['token'], "refresh_token" => $oUser['tokens']['refresh_token'] ], 200);
                    } else {
                        return $this->response(["error" => "forbidden user 3" . $refresh_token."--".$oUrl->token], 403);
                    }

                }
            } else {
                return $this->response(["error" => "forbidden user 2"], 403);
            }
        } else {
            return $this->response(["error" => "forbidden user 1"], 403);
        }

    }
}