<?php

namespace Knowledge\System;

use Knowledge\Models\UserToken;
use Knowledge\Models\User;

class Token
{
    static public function deleteUserTokenByToken($token) {
        $mUserToken = new UserToken();
        $mUserToken->delete(["token" => $token]);
    }
    static public function getUserByRefreshToken($token, $refresh_token)
    {
        $oUserToken = self::getUserTokenByToken($token, $refresh_token);
        if ($oUserToken) {
            $mUser = new User();
            $oUser = $mUser->find(["id" => $oUserToken['user_id']]);
            if ($oUser) {
                $oUser['tokens'] = self::genTokenForUser($oUser);
                return $oUser;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    static public function getUserByToken($token)
    {
        $token = htmlspecialchars(strip_tags(trim($token)));
        $oUserToken = self::getUserTokenByToken($token);
        if ($oUserToken) {
            $mUser = new User();
            $oUser = $mUser->find(["id" => $oUserToken['user_id']]);
            if ($oUser) {
                if (strtotime($oUserToken['exp_time']) < time()) {
                    $oUser['old'] = 1;
                }
                return $oUser;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
    static public function getUserTokenByToken($token, $refresh_token = "") {
        $mUserToken = new UserToken();
        $fnd_arr = [
            "token" => $token
        ];
        if ($refresh_token != "") {
            $fnd_arr['token_refresh'] = $refresh_token;
        }

        return $mUserToken->find($fnd_arr);
    }

    static public function genTokenForUser($oUser, $exp = 0) {
        $token = base64_encode(random_bytes(12));

        $mUserToken = new UserToken();
        $mUserToken->delete(["user_id" => $oUser['id']]);
        while ($mUserToken->checkTokenForDublicates($token)) {
            $token = base64_encode(random_bytes(12));
        }
        $refresh_token = base64_encode(random_bytes(12));
        $tm = time();
        if ($exp == 0) {
            $exp = $tm + 60 * 10;
        } else {
            $exp = $tm + 60 * 60 * 24 * 30;
        }
        $ins_arr = [
            "user_id" => $oUser['id'],
            "token" => $token,
            "token_refresh" => $refresh_token,
            "create_time" => date("Y-m-d H:i:s", $tm),
            "exp_time" => date("Y-m-d H:i:s", $exp),
        ];
        $mUserToken->insert($ins_arr);
        return [
            "token" => $token,
            "refresh_token" => $refresh_token,
            "exp" => $exp
        ];
    }
}