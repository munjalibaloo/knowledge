<?php
namespace Knowledge\Models;

use Knowledge\System\Db;

abstract class Model
{
    protected $table;
    protected $dbConnection;

    public function __construct()
    {
        $this->dbConnection = Db::getInstance()->getConnection();
    }

    function getConnection()
    {
        return $this->dbConnection;
    }

    function buildWhereArr($where)
    {
        $where_arr = [];
        foreach ($where as $name => $val) {
            if (is_array($val)) {
                $vals = [];
                foreach ($val as $val_single) {
                    $vals[] = $this->dbConnection->real_escape_string($val_single);
                }
                $val_str = implode("','", $vals);
                $where_arr[] = $name." IN ('".$val_str."')";
            } else {
                $where_arr[] = $name." = '".$this->dbConnection->real_escape_string($val)."'";
            }
        }
        return $where_arr;
    }

    public function find($where, $full = false) {
        $sql = "SELECT * FROM ".$this->table;
        $where_arr = $this->buildWhereArr($where);
        if ($where_arr) {
            $sql .= " WHERE ".implode(" && ", $where_arr);

            return $this->select($sql, $full);
        } else {
            return false;
        }
    }

    public function countAll($key = 'id') {
        $sql = "SELECT COUNT({$key}) as cnt FROM {$this->table} WHERE 1";
        $oRes =  $this->select($sql);
        if ($oRes) {
            return $oRes['cnt'];
        } else {
            return 0;
        }
    }
    public function insert($ins_arr)
    {
        if ($ins_arr) {
            $nms = [];
            $vals = [];
            foreach ($ins_arr as $nm => $val) {
                $nms[] = "`{$nm}`";
                $vals[] = "'".$this->dbConnection->real_escape_string($val)."'";
            }
            $sql = "INSERT INTO {$this->table} (".implode(",", $nms).") VALUES (".implode(',', $vals).")";

            if ($this->query($sql)) {
                return $this->dbConnection->insert_id;
            }
        }
    }

    public function delete($del_arr)
    {
        $sql = "DELETE FROM ".$this->table;
        $where_arr = $this->buildWhereArr($del_arr);
        if ($where_arr) {
            $sql .= " WHERE ".implode(" && ", $where_arr);
            return $this->query($sql);
        } else {
            return false;
        }
    }
    public function query($sql) {
        $oRes = $this->dbConnection->query($sql);
        return $oRes;
    }

    public function select($sql, $full = false)
    {
        $oRes = $this->query($sql);

        if ($oRes) {

            if ($full == true) {
                $aResSel = [];
                while ($oResSel = $oRes->fetch_assoc()) {
                    $aResSel[] = $oResSel;
                }
            } else {
                $aResSel = $oRes->fetch_assoc();
            }
            $oRes->close();
            return $aResSel;
        } else {
            return false;
        }
    }
}