<?php

namespace Knowledge\Models;

class Student extends Model
{
    protected $table = "students";

    function getNextStudents($page = 1, $limit = 5) {
        if ($page == 0) {
            $page = 1;
        }
        $offset = ($page - 1) * $limit;
        $sql = "SELECT * FROM {$this->table} WHERE 1 LIMIT {$offset}, {$limit}";
        return $this->select($sql, 1);
    }
}