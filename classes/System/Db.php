<?php

namespace Knowledge\System;

class Db
{
    private static $instance;
    private $dbConnection;

    private function __construct()
    {
        $oConfigDb = self::getConfigDb();
        $this->dbConnection = new \mysqli($oConfigDb['host'], $oConfigDb['user'], $oConfigDb['password'], $oConfigDb['database']);
        $this->dbConnection->set_charset($oConfigDb['charset']);
    }

    private function __clone () {}

    private function __wakeup () {}

    static function getConfigDb()
    {
        $path = str_replace('\\', '/', '__DIR__."../../config/')."db.php";
        return require_once $path;
    }

    public static function getInstance()
    {
        if(self::$instance === null) {
            self::$instance = new Db();
        }
        return self::$instance;
    }

    public function getConnection()
    {
        return $this->dbConnection;
    }
}