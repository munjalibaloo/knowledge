/*----js here--*/
function apiRequest(type, url, data_send) {
    return $.ajax({
        url: url,
        type: type,
        data: data_send,
        dataType: 'json',
        headers: {
            token: localStorage.getItem("token"),
            refreshtoken: localStorage.getItem("refresh_token")
        }
    });
}

function checkLoginForm() {
    apiRequest("get","/auth", {}).done(function(data) {
        if (data.id) {
            window.location.href = "users.html";
        }
        if (data.token) {
            localStorage.setItem("token", response.token);
            localStorage.setItem("refresh_token", response.refresh_token);
        }
    });
}

function checkUsersHtml() {
    apiRequest("get","/auth", {}).done(function(data) {
        if (data.token) {
            localStorage.setItem("token", response.token);
            localStorage.setItem("refresh_token", response.refresh_token);
        }
        if (data.id) {
            getStudents(0);
        }
    }).fail(function (data) {
        window.location.href = "login.html";
    });
}

function logoutUser() {
    apiRequest("delete","/auth", {}).done(function(data) {
        localStorage.removeItem('token');
        localStorage.removeItem('refresh_token');
        window.location.href = "login.html";
    });
}

function getStudents(pg) {
    apiRequest("get","/users", {page: pg}).done(function(data) {
        if (data.aStudents) {
            showStudentsHtml(data.aStudents);
        }
        if (data.pages_count > 1) {
            showStudentsPagination(data.pages_count, data.page);
        }
    }).fail(function (data) {
        checkUsersHtml();
    });
}
function showStudentsPagination(pages_count, page) {
    var ul = $("<ul />").addClass("pagination");
    if (page > 1) {
        ul.append("<li><a href='javascript:void(null)' onClick='getStudents(" + (+page - 1) + ")'>&laquo; Prev</a></li>");
    }
    for (i = 1; i <= pages_count; i++) {
        if (i == page || (page ==  0 &&  i == 1)) {
            ul.append("<li class='current'>" + i + "</li>");
        } else {
            ul.append("<li><a href='javascript:void(null)' onClick='getStudents(" + i + ")'>" + i + "</a></li>");
        }
    }
    if (page < pages_count) {
        ul.append("<li><a href='javascript:void(null)' onClick='getStudents(" + (+page + 1) + ")'>Next &raquo;</a></li>");
    }


    $(".users__pagin").html(ul);
}

function showStudentsHtml(aStudents) {
    $(".users__table").html("");
    $.each(aStudents, function(ind, student) {
        var div = $("<div />").addClass("row align-items-center");
        if (ind %  2 == 0) {
            div.addClass("bgc-light");
        } else {
            div.addClass("bgc-dark");

        }
        div.append("<div class='col-1 offset-1 users__check'><img src=\"images/checkcircle.svg\" alt=\"\"></div>");
        div.append("<div class='col-7'><ul  class=\"users__student\">" +
            "<li>" + student.username + "</li>" +
            "<li>" + student.name + " " + student.last_name + "</li>" +
            "</ul></div>");
        div.append("<div class='col-3'><ul  class=\"users__group\">" +
            "<li>...</li>" +
            "<li>Default Group</li>" +
            "</ul></div>");
        $(".users__table").append(div);
    });
}
$(document).ready(function() {

    $("body").on("submit", ".j-form-login", function(ev) {
        ev.preventDefault();
        var f = $(this);
        f.find(".is-invalid").removeClass("is-invalid");
        f.find(".alert").remove();
        var username = f.find("input[name='username']");
        var password = f.find("input[name='password']");
        if (username.val() != '' && password.val() != '') {
            $.post(f.attr("action"), f.serialize(), function (response) {
                if (response.token) {
                    localStorage.setItem("token", response.token);
                    localStorage.setItem("refresh_token", response.refresh_token);
                    window.location.href = "users.html";
                }
            }, "json").fail(function(response) {

                var json_ans = response.responseJSON;
                if (json_ans.error) {
                    var alert_htm = $('<div class="alert alert-danger" role="alert">' + json_ans.error + '</div>');
                    f.prepend(alert_htm);
                }

            });
        } else {
            if (username.val() == "") {
                username.addClass("is-invalid");
            }
            if (password.val() == "") {
                password.addClass("is-invalid");
            }
        }

    });
});